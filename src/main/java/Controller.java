import java.util.Scanner;

/**
 * Created by marcin on 27.06.17.
 */
public class Controller {

    public void setVariables(){
        //setting Word filename
        Scanner scanner=new Scanner(System.in);
        System.out.println("Podaj nazwe pliku WORD: ");
        String wordFileName=scanner.nextLine();
        new Word().WORD_INPUT_FILE=wordFileName;

        //setting Excel filename
        Excel excel=new Excel();
        System.out.println("Podaj nazwe pliku EXCEL: ");
        String excelFilename=scanner.nextLine();
        excel.setEXCEL_INPUT_FILE(excelFilename);
    }
}
