import org.apache.poi.ss.usermodel.*;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

/**
 * Created by marcin on 18.06.17.
 */
public class Excel{
    public static final int DISPLACEMENT_OF_INDEX = 1;
    public static final int ASCII_DISPLACEMENT = 96;
    public String EXCEL_INPUT_FILE;
    public int COUNT_OF_ROWS_TO_IGNORE;
    public int COLUMN_WITH_NAMES;
    public int COLUMN_WITH_ADRESS;
    public int COLUMN_WITH_FLAT_NUMBERS;
    //TODO: add method with scanner to get column with keywords
    public List<String> listOfNames =new ArrayList<>();
    public List<String> listOfAdress =new ArrayList<>();
    public List<String> getListOfNames() {
        return listOfNames;
    }
    public List<String> getListOfAdress() {
        return listOfAdress;
    }
    public List<String> listOfFlatNumbers=new ArrayList<>();
    public List<String> getListOfFlatNumbers() {
        return listOfFlatNumbers;
    }
    public void setEXCEL_INPUT_FILE(String EXCEL_INPUT_FILE) {
        this.EXCEL_INPUT_FILE = EXCEL_INPUT_FILE;
    }

    public void readFromExcelFile() {
        try {
            FileInputStream excelFile = new FileInputStream(new File(EXCEL_INPUT_FILE));
            Workbook workbook = new XSSFWorkbook(excelFile);
            Sheet datatypeSheet = workbook.getSheetAt(0);
            Iterator<Row> iterator = datatypeSheet.iterator();
            while (iterator.hasNext()) {
                Row currentRow = iterator.next(); //??
                Iterator<Cell> cellIterator = currentRow.iterator(); //??
                while (cellIterator.hasNext()) {
                    Cell currentCell = cellIterator.next();
                    //ignoring first row!!!
                    if (currentCell.getRowIndex()<=COUNT_OF_ROWS_TO_IGNORE-1){
                       //stringsToReplaceInWord.put(currentCell.getStringCellValue(),null);
                        break;
                    }
                    if (currentCell.getCellTypeEnum() == CellType.STRING && currentCell.getColumnIndex()==COLUMN_WITH_NAMES-1) {
                        listOfNames.add(currentCell.getStringCellValue());//add current cell to the list
                        //how to add this cell to list of strings???
                    }else if (currentCell.getCellTypeEnum() == CellType.STRING && currentCell.getColumnIndex()==COLUMN_WITH_ADRESS-1){
                        listOfAdress.add(currentCell.getStringCellValue());
                    }
                    else if (currentCell.getColumnIndex()==COLUMN_WITH_FLAT_NUMBERS-1) {
                        if (currentCell.getCellTypeEnum() == CellType.NUMERIC){
                            double currentNumericalCellValue=currentCell.getNumericCellValue();
                            String currentNumericalCellValueToString=String.format("%.0f",currentNumericalCellValue);
                            listOfFlatNumbers.add(currentNumericalCellValueToString);
                        }else {
                            listOfFlatNumbers.add(currentCell.getStringCellValue());
                        }

                    }
                }
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setExcelVariables() {
        //setting FILENAME
        Scanner scanner=new Scanner(System.in);
        System.out.println("Podaj nazwe pliku EXCEL: ");
        String excelFilename=scanner.nextLine();
        EXCEL_INPUT_FILE=excelFilename;
        //setting ROWS TO IGNORE
        System.out.println("Podaj numer wiersza ze slowami kluczowymi (imie i nazwisko, nr mieszkania itd..). ");
        COUNT_OF_ROWS_TO_IGNORE=scanner.nextInt()- DISPLACEMENT_OF_INDEX;
        System.out.println("Podaj litere kolumny z imionami i nazwiskami: ");
        //COLUMN_WITH_NAMES=scanner.nextInt();//without displacement becouse columns are count from 1
        COLUMN_WITH_NAMES=(int)scanner.next(".").toLowerCase().charAt(0)- ASCII_DISPLACEMENT;
        System.out.println("Podaj litere kolumny z numerami mieszkan: ");
        COLUMN_WITH_FLAT_NUMBERS=(int)scanner.next(".").toLowerCase().charAt(0)- ASCII_DISPLACEMENT;
        System.out.println("Podaj litere kolumny z nazwami ulic: ");
        COLUMN_WITH_ADRESS=(int)scanner.next(".").toLowerCase().charAt(0)- ASCII_DISPLACEMENT;
    }
    public void setExcelVariables(String excelFile,String[] numbersOfColumns) {
        //setting FILENAME
        Scanner scanner=new Scanner(System.in);
//        System.out.println("Podaj nazwe pliku EXCEL: ");
//        String excelFilename=scanner.nextLine();
        EXCEL_INPUT_FILE=excelFile;
        //setting ROWS TO IGNORE
        COUNT_OF_ROWS_TO_IGNORE=Integer.parseInt(numbersOfColumns[0])- DISPLACEMENT_OF_INDEX;
        COLUMN_WITH_NAMES=numbersOfColumns[1].toLowerCase().charAt(0)-ASCII_DISPLACEMENT;
        COLUMN_WITH_FLAT_NUMBERS=numbersOfColumns[2].toLowerCase().charAt(0)-ASCII_DISPLACEMENT;
        COLUMN_WITH_ADRESS=numbersOfColumns[3].toLowerCase().charAt(0)-ASCII_DISPLACEMENT;
    }
}