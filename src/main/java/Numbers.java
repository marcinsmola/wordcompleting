import java.util.ArrayList;
import java.util.List;

/**
 * Created by marcin on 19.06.17.
 */
public class Numbers {
    private int number1 = 50;
    private int number2 = 100;
    private List<Integer> list;

    public Numbers() {
        list = new ArrayList<Integer>();
        list.add(number1);
        list.add(number2);
    }

    public List<Integer> getList() {
        return list;
    }
}
