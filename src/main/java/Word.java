import org.apache.commons.io.FilenameUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.xwpf.usermodel.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.*;

/**
 * Created by marcin on 18.06.17.
 */
public class Word {
    public String WORD_INPUT_FILE;
    /*TODO: add converting whole files in folder(docx->pdf),
       TODO: add merging all pdfs to 1 file
       TODO: add simple gui (selecting files, asking for default settings (select columns or no)
       */
    List<String> nameOfFileslist = new ArrayList<>();
    List<String> listOfKeywords = new ArrayList<String>();
    Excel excel = new Excel();

    public void printElementsOfExcelLists() {
        excel.readFromExcelFile();
        for (int i = 0; i < excel.getListOfNames().size(); i++) {
            System.out.println(excel.getListOfNames().get(i));
            System.out.println(excel.getListOfFlatNumbers().get(i));
            System.out.println(excel.getListOfAdress().get(i));
        }
    }

    public void addElementsToKeywordsList() {
        excel.readFromExcelFile();
        listOfKeywords.add(excel.getListOfNames().get(0));
        listOfKeywords.add(excel.getListOfAdress().get(0));
        listOfKeywords.add(excel.getListOfFlatNumbers().get(0));
    }

    public void wordTextReplacement() throws InvalidFormatException {
        excel.setExcelVariables();
        //excel.readFromExcelFile();

        addElementsToKeywordsList();
        final String STREET_NAME = excel.getListOfAdress().get(0);
        final String NAME_AND_SURNAME = excel.getListOfNames().get(0);
        final String FLAT_NUMBER = excel.getListOfFlatNumbers().get(0);
        final String directoryName = excel.getListOfAdress().get(1) + "--" + FilenameUtils.getBaseName(WORD_INPUT_FILE);
        File dir = new File(directoryName); //creating a folder
        dir.mkdir();
        for (int i = 0; i < excel.listOfFlatNumbers.size(); i++) {//how many names
            try {
                XWPFDocument doc = new XWPFDocument(OPCPackage.open(WORD_INPUT_FILE));
                for (XWPFParagraph p : doc.getParagraphs()) {
                    List<XWPFRun> runs = p.getRuns();
                    if (runs != null) {
                        for (XWPFRun r : runs) {
                            String text = r.getText(0);
                            if (text != null && (text.contains(NAME_AND_SURNAME))) { //names
                                text = text.replace(NAME_AND_SURNAME, excel.getListOfNames().get(i));
                                r.setText(text, 0);
                            }
                            if (text != null && (text.contains(STREET_NAME))) { //adressess
                                text = text.replace(STREET_NAME, excel.getListOfAdress().get(i));
                                r.setText(text, 0);
                            }
                            if (text != null && text.contains(FLAT_NUMBER)) { //flat numbers
                                text = text.replace(FLAT_NUMBER, excel.getListOfFlatNumbers().get(i));
                                r.setText(text, 0);
                            }
                        }
                    }
                }

                File file = new File(directoryName + File.separator +
                        excel.getListOfFlatNumbers().get(i) + ".docx");//creating file in directory
                doc.write(new FileOutputStream(file));

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    public void setWordVariables(String filepath) {
//        Scanner scanner = new Scanner(System.in);
//        System.out.println("Podaj nazwe pliku WORD: ");
//        String wordFileName = scanner.nextLine();
        WORD_INPUT_FILE = filepath;
    }
    public void setWordVariables() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj nazwe pliku WORD: ");
        String wordFileName = scanner.nextLine();
        WORD_INPUT_FILE = wordFileName;
    }
    public void wordTextReplacement(String excelFile,String[] numberOfColumns) throws InvalidFormatException {
        excel.setExcelVariables(excelFile,numberOfColumns);
        excel.readFromExcelFile();

        addElementsToKeywordsList();
        final String STREET_NAME = excel.getListOfAdress().get(0);
        final String NAME_AND_SURNAME = excel.getListOfNames().get(0);
        final String FLAT_NUMBER = excel.getListOfFlatNumbers().get(0);
        final String directoryName = excel.getListOfAdress().get(1) + "--" + FilenameUtils.getBaseName(WORD_INPUT_FILE);
        File dir = new File(directoryName); //creating a folder
        dir.mkdir();
        System.out.println("Directory created"+directoryName);
        for (int i = 0; i < excel.listOfFlatNumbers.size(); i++) {//how many names
            try {
                XWPFDocument doc = new XWPFDocument(OPCPackage.open(WORD_INPUT_FILE));
                for (XWPFParagraph p : doc.getParagraphs()) {
                    List<XWPFRun> runs = p.getRuns();
                    if (runs != null) {
                        for (XWPFRun r : runs) {
                            String text = r.getText(0);
                            if (text != null && (text.contains(NAME_AND_SURNAME))) { //names
                                text = text.replace(NAME_AND_SURNAME, excel.getListOfNames().get(i));
                                r.setText(text, 0);
                            }
                            if (text != null && (text.contains(STREET_NAME))) { //adressess
                                text = text.replace(STREET_NAME, excel.getListOfAdress().get(i));
                                r.setText(text, 0);
                            }
                            if (text != null && text.contains(FLAT_NUMBER)) { //flat numbers
                                text = text.replace(FLAT_NUMBER, excel.getListOfFlatNumbers().get(i));
                                r.setText(text, 0);
                            }
                        }
                    }
                }

                File file = new File(directoryName +File.separator +
                        excel.getListOfFlatNumbers().get(i) + ".docx");//creating file in directory
                doc.write(new FileOutputStream(file));

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }


}
