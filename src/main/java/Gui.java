import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;

/**
 * Created by marcin on 06.07.17
 * .
 */
public class Gui {
    private JPanel mainPanel;
    private JButton wordFileChooser;
    private JLabel wordFileSelecting;
    private JButton znajdźPlikExcelButton;
    private JTextField keywords;
    private JTextField flatNumbers;
    private JTextField streetNames;
    private JTextField names;
    private JButton startButton;
    private JLabel wordPath;
    private JLabel excelPath;
    final String[] selectedFiles = new String[2];
    String excelFilePath;
    String wordFilePath;
    public Gui() {
        wordFileChooser.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    selectedFiles[0] = fileChooser.getSelectedFile().getPath();
                    wordFilePath =selectedFiles[0];
                    int index = wordFilePath.lastIndexOf(File.separator);
                    String nameOfWordFile=wordFilePath.substring(index+1);
                    wordPath.setText(nameOfWordFile);
                    System.out.println(wordFilePath);//word file path
                }
            }
        });
        znajdźPlikExcelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                int returnValue = fileChooser.showOpenDialog(null);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    selectedFiles[1] = fileChooser.getSelectedFile().getPath();
                    excelFilePath =selectedFiles[1];
                    int index = excelFilePath.lastIndexOf(File.separator);
                    String nameOfExcelFile=excelFilePath.substring(index+1);
                    excelPath.setText(nameOfExcelFile);
                    System.out.println(excelFilePath);//excel file path
                }
            }
        });
        startButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    System.out.println("GO!");
                    String text = keywords.getText();
                    System.out.println(text);
                    Word word = new Word();
                    //excel.createFile();
                    //excel.readFromExcelFile();
                    //test.printList();
                    //excel.readFromExcelFile();
                    word.setWordVariables(wordFilePath);
                    String[] numbersOfColumns = getValueFromTextFields();
                    try {

                        word.wordTextReplacement(excelFilePath, numbersOfColumns);
                        word.printElementsOfExcelLists();
                    } catch (InvalidFormatException e1) {
                        e1.printStackTrace();
                    }
                }catch (IndexOutOfBoundsException e1){
                    JOptionPane.showMessageDialog(null, "Dziękuję.", null, 1);

                }
                //word.printElementsOfExcelLists();

            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("By Marcin Smoła");
        frame.setResizable(true);
        frame.setContentPane(new Gui().mainPanel);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
        frame.setLocationRelativeTo(null);
        frame.setLayout(new BorderLayout());
    }

    public String[] getValueFromTextFields(){
        String[] values=new String[4];
        values[0]=keywords.getText();
        values[1]=names.getText();
        values[2]=flatNumbers.getText();
        values[3]=streetNames.getText();
        return values;
    }
}
